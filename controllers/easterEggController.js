var easterEggs = require('../data/mockedData');


exports.list = function(req, res){
  
  var message, idValid = false;
  var easterEgg;
  for(easterEgg of easterEggs){
  	if(easterEgg.id == req.params.eggId){
  	  idValid = true;
  	  break;	
  	}
  }
  if(idValid){
  	res.status(200).json({
  	  easterEgg
  	});
  }else{
  	message = "Data not found";
  	res.status(403).json({
  		message
  	});
  }
  res.end();
};

exports.listall = function(req, res){
  
  res.status(200).json(easterEggs);	
  res.end();
};

