var express = require('express');
var api = require('./routes/api');
var app = express();
var bodyParser= require('body-parser');
var jwt=require('jsonwebtoken');

var easter_egg_controller = require('./controllers/easterEggController');

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({     
    extended: true
}));

app.use(api);

/*app.use((req, res, next)=>{
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if(token){
    //Decode the token
    jwt.verify(token,"samplesecret",(err,decod)=>{
      if(err){
        res.status(403).json({
          message:"Wrong Token"
        });
      }
      else{
        //If decoded then call next() so that respective route is called.
        req.decoded=decod;
        next();
      }
    });
  }
  else{
    res.status(403).json({
       message:"No Token"
    });
  }
});*/

app.listen(3000, function(){
	console.log('listening on port 3000');
});