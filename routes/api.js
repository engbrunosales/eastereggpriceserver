var express = require('express');
var router = express.Router();


//Contollers
var user_controller = require('../controllers/userController');
var easter_egg_controller = require('../controllers/easterEggController');


// Implement your services here //
router.post('/api/v1/auth', user_controller.user_login);
router.delete('/api/v1/auth', user_controller.user_logout);


router.get('/api/v1/easter-eggs', easter_egg_controller.listall);
router.get('/api/v1/easter-eggs/:eggId', easter_egg_controller.list);


module.exports = router;
