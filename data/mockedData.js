
// Insira mais exemplos aqui para enriquecer o seu projeto
module.exports = [
  {
    id: 1,
    nome: 'Nestlé Classic',
    marca: 'Nestlé',
    sabor: 'Chocolate ao leite',
    peso: 240,
    preco: 35.00,
    imagem: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k'
  },
  {
    id: 2,
    nome: 'Nestlé Classic2',
    marca: 'Nestlé',
    sabor: 'Chocolate ao leite',
    peso: 240,
    preco: 35.00,
    imagem: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k'
  },
  {
    id: 3,
    nome: 'Nestlé Classic3',
    marca: 'Nestlé',
    sabor: 'Chocolate ao leite',
    peso: 240,
    preco: 35.00,
    imagem: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k'
  }
];